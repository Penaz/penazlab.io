#!/usr/bin/env python3
import jinja2
from sys import argv
from shutil import copytree, copyfile
from os.path import exists, isdir
from os import remove, mkdir
import yaml

_STD_ = "dist/"

if (exists("dist/projects")):
    if (not isdir("dist/projects")):
        remove("dist/projects")
else:
    mkdir("dist/projects")

if len(argv) > 1:
    if (argv[1] in ["--local", "-l"]):
        print("Copying missing files")
        try:
            copytree("./images", "dist/images")
            copytree("./style", "dist/style")
            copytree("./scripts", "dist/scripts")
            copyfile("./templates/lightbox.html", "dist/lightbox.html")
        except OSError as exc:
            print("Error while locally deploying" + exc)

with open("templates/projects.yml", "r") as f:
    projList = yaml.load(f, Loader=yaml.CLoader)

with open("templates/skilllist.yml", "r") as f:
    skillList = yaml.load(f, Loader=yaml.CLoader)

pages = {
    "index.html": {
        "title": "Daniele Penazzo (Penaz)",
        "outpath": ".",
        "project": False,
        "variables": {
            "projects": projList,
            "skills": skillList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Welcome to my Website, I'm Daniele Penazzo and this web space is about me and my projects.",
            "cardimage": "/images/me.png",
            "cardimagealt": "Picture of Daniele Penazzo, the owner and creator of this website."
            }
        },
    "about.html": {
        "title": "About Me - Daniele Penazzo (Penaz)",
        "outpath": ".",
        "project": False,
        "variables": {
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "More information about Daniele Penazzo, the owner and creator of this website.",
            "cardimage": "/images/me.png",
            "cardimagealt": "Picture of Daniele Penazzo, the owner and creator of this website."
        }
    },
    "projects.html": {
        "title": "Projects - Daniele Penazzo (Penaz)",
        "outpath": ".",
        "project": False,
        "variables": {
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web page about my most important projects developed so far.",
            "cardimage": "/images/cards/this.png",
            "cardimagealt": "A generic picture of a stylized white globe on a gray background."
        }
    },
    "projects/npass.html": {
        "title": "Npass - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of NPass: a fuzzy finder / Ncurses-based interface for pass - the standard Unix password manager.",
            "cardimage": "/images/cards/NPass.png",
            "cardimagealt": "A logo used for the nPass software."
        }
    },
    "projects/atodo.html": {
        "title": "aTodo - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of aTodo: a simple JSON-based ToDo keeper made in Python 3.",
            "cardimage": "/images/cards/atodo.png",
            "cardimagealt": "A logo used for the aTodo software."
        }
    },
    "projects/packt.html": {
        "title": "PacktPubExplorer - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web page on PacktPubExplorer: a small scraper made in Python that retrieves today's free ebook at Packt Publishing.",
            "cardimage": "/images/cards/packt.png",
            "cardimagealt": "A logo used for the PacktPubExplorer software."
        }
    },
    "projects/this.html": {
        "title": "This Website - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "An informational page about the project that is this very website.",
            "cardimage": "/images/cards/this.png",
            "cardimagealt": "A generic picture of a stylized white globe on a gray background."
        }
    },
    "projects/VC.html": {
        "title": "VoiceChanger - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web page about VoiceChanger: a small TkInter GUI for Sox, made in Python.",
            "cardimage": "/images/cards/VC.png",
            "cardimagealt": "A logo used for the VoiceChanger software."
        }
    },
    "projects/MC.html": {
        "title": "Minecraft Plugins - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page about the small Minecraft Bukkit/Spigot plugins I made in the past.",
            "cardimage": "/images/cards/MC.png",
            "cardimagealt": "A logo used for the Minecraft Plugins Page."
        }
    },
    "projects/2dgd_f0th.html": {
        "title": "2D Game Development: From Zero To Hero - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of the ebook '2D Game Development: From Zero to Hero'. An almost no-assumptions book that teaches people how to develop videogames from the ground up.",
            "cardimage": "/images/cards/comingsoon.png",
            "cardimagealt": "The logo for this page will be coming soon."
        }
    },
    "projects/lazur.html": {
        "title": "Lazur - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of Lazur: my 'Object Oriented Programming' class project that lasted 50 hours.",
            "cardimage": "/images/cards/comingsoon.png",
            "cardimagealt": "The logo for this page will be coming soon."
        }
    },
    "projects/GH.html": {
        "title": "Glitch_Heaven - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of Glitch_Heaven, an experimental video game where you use willingly-inserted glitches to get to the next level.",
            "cardimage": "/images/cards/GH.png",
            "cardimagealt": "A logo used for Glitch_Heaven."
        }
    },
    "projects/entostudio.html": {
        "title": "EntoStudio - Daniele Penazzo (Penaz)",
        "outpath": "./projects/",
        "project": True,
        "variables": {
            "projects": projList,
            "twittercard": "summary",
            "twitterhandle": "@penazarea",
            "description": "Web Page of EntoStudio: a mosquito tracker with statistics and reporting features.",
            "cardimage": "/images/cards/comingsoon.png",
            "cardimagealt": "The logo for this page will be coming soon."
        }
    }
}

render_environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader("templates"))

for filename, content in pages.items():
    print(content["title"])
    output = render_environment.get_template(filename).render(
            content['variables'],
            pageTitle=content["title"],
            project=content["project"],
            outpath=_STD_ + content["outpath"]
            )
    with open(_STD_ + filename, "w") as result:
        result.write(output)
