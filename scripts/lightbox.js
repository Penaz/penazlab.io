function openLightbox(imgUrl){
    var img = document.getElementById("lightboximg");
    var lb = document.getElementById("lightboxBG");
    img.src = imgUrl;
    lb.style.display = "block";
    return false;
}

function closeLightbox(){
    var lb = document.getElementById("lightboxBG");
    lb.style.display = "none";
}

function prepareLightbox(){
    var request = new XMLHttpRequest();
    request.onreadystatechange=function(){
        if (this.readyState == 4 && this.status == 200){
            document.getElementById("lightboxcontainer").innerHTML=this.responseText;
            var lbclose = document.getElementById("lightboxClose");
            lbclose.onclick = function(){closeLightbox();};
            var lbbg = document.getElementById("lightbox");
            lbbg.onclick = function(){closeLightbox();};
            document.close();
        }
    }
    request.open("GET", "/lightbox.html", true);
    request.send();
}

window.onload = function(){
    var lightboxedimgs = document.getElementsByClassName("lightboximg");
        if (lightboxedimgs.length != 0){
            prepareLightbox();
            for (var i=0; i < lightboxedimgs.length; i++){
                lightboxedimgs[i].onclick = function(){openLightbox(this.getAttribute("href")); return false;};
            }
        }
}
